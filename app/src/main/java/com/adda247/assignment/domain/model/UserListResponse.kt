package com.adda247.assignment.domain.model

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class UserListResponse(
    var code: Int?,
    var data: List<Data>?,
    var meta: Meta?
)

@JsonClass(generateAdapter = true)
data class Data(
    var created_at: String?,
    var email: String?,
    var gender: String?,
    var id: Int?,
    var name: String?,
    var status: String?,
    var updated_at: String?
)

@JsonClass(generateAdapter = true)
data class Meta(
    var pagination: Pagination?
)

@JsonClass(generateAdapter = true)
data class Pagination(
    var limit: Int?,
    var page: Int?,
    var pages: Int?,
    var total: Int?
)