package com.adda247.assignment.domain.usecase

import com.adda247.assignment.data.repository.HomeRemoteRepository
import com.adda247.assignment.data.source.remote.ApiErrorHandle
import com.adda247.assignment.domain.model.UserListResponse
import com.adda247.assignment.domain.usecase.base.SingleUseCase

/**
 * An interactor that calls the actual implementation of [NotesViewModel](which is injected by DI)
 * it handles the response that returns data &
 * contains a list of actions, event steps
 */
class UserListUseCase(
    private val homeRemoteRepository: HomeRemoteRepository,
    apiErrorHandle: ApiErrorHandle
) : SingleUseCase<UserListResponse, Any?>(apiErrorHandle) {

    override suspend fun run(params: Any?): UserListResponse {
        return homeRemoteRepository.getUsers(page = params as Int)
    }
}