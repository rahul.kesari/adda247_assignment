package com.adda247.assignment.domain.usecase.base

import com.adda247.assignment.data.source.remote.ErrorModel

interface UseCaseResponse<Type> {

    fun onSuccess(result: Type)

    fun onError(errorModel: ErrorModel?)
}
