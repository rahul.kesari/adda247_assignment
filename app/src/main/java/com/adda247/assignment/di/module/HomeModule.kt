package com.adda247.assignment.di.module

import com.adda247.assignment.data.repository.HomeLocalRepository
import com.adda247.assignment.data.repository.HomeRemoteRepository
import com.adda247.assignment.data.source.local.AppDatabase
import com.adda247.assignment.data.source.remote.APIService
import com.adda247.assignment.data.source.remote.ApiErrorHandle
import com.adda247.assignment.domain.usecase.UserListUseCase
import com.adda247.assignment.presentation.users.UsersViewModel
import com.adda247.assignment.presentation.home.HomeViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val HomeModule = module {

    viewModel { HomeViewModel() }

    viewModel { UsersViewModel(get()) }

    single { createGetAlbumsUseCase(get(), createApiErrorHandle()) }

    single { createHomeLocalRepository(get()) }

    single { createHomeRemoteRepository(get()) }
}


fun createHomeLocalRepository(database: AppDatabase): HomeLocalRepository {
    return HomeLocalRepository(
        database
    )
}

fun createHomeRemoteRepository(apiService: APIService): HomeRemoteRepository {
    return HomeRemoteRepository(
        apiService
    )
}

fun createGetAlbumsUseCase(
    homeRemoteRepository: HomeRemoteRepository,
    apiErrorHandle: ApiErrorHandle
): UserListUseCase {
    return UserListUseCase(homeRemoteRepository, apiErrorHandle)
}