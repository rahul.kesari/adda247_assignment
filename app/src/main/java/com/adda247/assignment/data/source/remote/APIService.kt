package com.adda247.assignment.data.source.remote

import com.adda247.assignment.domain.model.UserListResponse
import retrofit2.http.GET
import retrofit2.http.Query

interface APIService {

    @GET("public-api/users/")
    suspend fun getUsers(@Query("page") page:Int): UserListResponse

}