package com.adda247.assignment.data.repository

import com.adda247.assignment.data.source.remote.APIService
import com.adda247.assignment.domain.model.UserListResponse

class HomeRemoteRepository(val apiService: APIService) {

    suspend fun getUsers(page:Int):UserListResponse{
        return apiService.getUsers(page)
    }
}