package com.adda247.assignment.data.source.local

import androidx.room.Database
import androidx.room.RoomDatabase
import com.adda247.assignment.data.model.UserEntity
import com.adda247.assignment.data.source.local.dao.UserDao


@Database(entities = [UserEntity::class], version = 1, exportSchema = false)
abstract class AppDatabase : RoomDatabase() {

    abstract val photoDao: UserDao

    companion object {
        const val DB_NAME = "AppDatabase.db"
    }
}
