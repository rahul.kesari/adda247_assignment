package com.adda247.assignment.presentation.home

import android.os.Bundle
import com.adda247.assignment.R
import com.adda247.assignment.databinding.ActivityHomeBinding
import com.adda247.assignment.presentation.users.UsersListFragment
import com.adda247.assignment.presentation.base.BaseActivity
import com.adda247.assignment.util.extensions.TransitionAnimation
import com.adda247.assignment.util.extensions.newFragmentInstance
import org.koin.android.viewmodel.ext.android.viewModel

class HomeActivity : BaseActivity<HomeViewModel, ActivityHomeBinding>() {

    override var frameContainerId: Int = R.id.homeContainer
    override val layoutId: Int = R.layout.activity_home
    override val viewModel: HomeViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        navigateTo(
            newFragmentInstance<UsersListFragment>(),
            UsersListFragment.FRAGMENT_NAME,
            TransitionAnimation.FADE,
            false
        )
    }
}
