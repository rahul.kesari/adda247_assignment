package com.adda247.assignment.presentation.users

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.adda247.assignment.MainApplication
import com.adda247.assignment.data.source.remote.ErrorModel
import com.adda247.assignment.domain.model.UserListResponse
import com.adda247.assignment.domain.usecase.UserListUseCase
import com.adda247.assignment.domain.usecase.base.UseCaseResponse
import com.adda247.assignment.presentation.base.BaseViewModel
import com.adda247.assignment.util.helper.NetworkHelper
import kotlinx.coroutines.ExperimentalCoroutinesApi
import java.io.IOException


/**
 * An interactor that calls the actual implementation of [AlbumViewModel](which is injected by DI)
 * it handles the response that returns data &
 * contains a list of actions, event steps
 */
class UsersViewModel constructor(private val userListUseCase: UserListUseCase) : BaseViewModel() {

    val TAG = UsersViewModel::class.java.name
    val mSnackbarText = MutableLiveData<String>()
    val usersLiveData = MutableLiveData<UserListResponse>()
    val isProcessing = MutableLiveData<Boolean>()

    @ExperimentalCoroutinesApi
    fun loadUsers(page:Int) {

            isProcessing.value = true
            userListUseCase.invoke(scope, page, object : UseCaseResponse<UserListResponse> {
                override fun onSuccess(result: UserListResponse) {
                    Log.i(TAG, "result: $result")
                    isProcessing.value = false
                    usersLiveData.value = result
                }

                override fun onError(errorModel: ErrorModel?) {
                    isProcessing.value = false
                    mSnackbarText.postValue(errorModel?.message)
                }

            })

    }


}