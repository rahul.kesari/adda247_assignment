package com.adda247.assignment.presentation.users

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.adda247.assignment.databinding.HolderUserBinding
import com.adda247.assignment.databinding.ProgressbarItemBinding
import com.adda247.assignment.domain.model.Data
import com.adda247.assignment.domain.model.UserListResponse

/**
 * Created by rahul
 */

class UserListAdapter(
    private val datas: List<Data?>
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    companion object {
        const val VIEW_PROG = 1
        const val VIEW_ITEM = 0
    }

    private lateinit var mContext: Context


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        mContext = parent.context
        if (viewType == VIEW_ITEM) {
            val itemBinding =
                HolderUserBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
            return UsersViewHolder(itemBinding)
        } else {
            val itemBinding =
                ProgressbarItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
            return ProgressViewHolder(itemBinding)
        }

    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is UsersViewHolder) {

            val data = (datas[position] as Data)
            holder.itemBinding.nameTV.text = data.name
            holder.itemBinding.emailTV.text = data.email
            holder.itemBinding.genderTV.text = data.gender
            holder.itemBinding.statusTV.text = data.status

        } else {
            (holder as ProgressViewHolder).bind()
        }
    }

    override fun getItemCount(): Int {
        return datas.size
    }


    override fun getItemViewType(position: Int): Int {
        return if (datas.get(position) != null) VIEW_ITEM else VIEW_PROG
    }


    class UsersViewHolder(val itemBinding: HolderUserBinding) :
        RecyclerView.ViewHolder(itemBinding.root) {
    }

    class ProgressViewHolder(private val itemBinding: ProgressbarItemBinding) :
        RecyclerView.ViewHolder(itemBinding.root) {
        fun bind() {
            Log.d("ProgressViewHolder", "progressbar ")
            itemBinding.progressBar1.setIndeterminate(true)
        }
    }
}

