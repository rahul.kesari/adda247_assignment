package com.adda247.assignment.presentation.base

interface LoginResultListener {

    fun onResult(status: Boolean)
}
