package com.adda247.assignment.presentation.base

import android.widget.ProgressBar

interface ProgressBarManager {
    val progressBar: ProgressBar?
}