package com.adda247.assignment.presentation.users

import android.os.Bundle
import android.view.View
import android.widget.ProgressBar
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.adda247.assignment.databinding.FragmentUsersListBinding
import com.adda247.assignment.domain.model.Data
import com.adda247.assignment.presentation.base.BaseFragment
import com.adda247.assignment.util.helper.EndlessRecyclerViewScrollListener
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.koin.android.viewmodel.ext.android.viewModel

class UsersListFragment : BaseFragment<UsersViewModel, FragmentUsersListBinding>() {

    private var adapter: UserListAdapter? = null
    override val viewModel: UsersViewModel by viewModel()
    override var title: String = "Users"
    override var menuId: Int = 0
    override val toolBarId: Int = 0
    override val layoutId: Int = com.adda247.assignment.R.layout.fragment_users_list
    override val progressBar: ProgressBar? = null
    private var usersList = ArrayList<Data?>()
    private var scrollListener: EndlessRecyclerViewScrollListener? = null
    private var totalPage = 0
    private var currentPage = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        observeViewModel()
    }

    @ExperimentalCoroutinesApi
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initUsersListRecycler()

        loadUsers()
    }

    private fun initUsersListRecycler() {
        adapter = UserListAdapter(usersList)
        val linearLayoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        binding.homeRecyclerView.layoutManager = linearLayoutManager

        scrollListener = object : EndlessRecyclerViewScrollListener(linearLayoutManager) {
            override fun onLoadMore(page: Int, totalItemsCount: Int, view: RecyclerView?) {
                if (currentPage < totalPage) {
                    currentPage += 1
                    binding.homeRecyclerView.post(Runnable {
                        usersList.add(null)
                        adapter?.notifyItemInserted(adapter?.itemCount!!)
                        loadUsers()
                    })
                }
            }
        }
        binding.homeRecyclerView.addOnScrollListener(scrollListener as EndlessRecyclerViewScrollListener)

        binding.homeRecyclerView.adapter = adapter
    }

    fun observeViewModel() {

        with(viewModel) {

            isProcessing.observe(this@UsersListFragment, Observer {
                if (it) binding.progressBar.visibility =
                    View.VISIBLE else binding.progressBar.visibility = View.GONE
            })


            usersLiveData.observe(this@UsersListFragment, Observer {
                if (it.code == 200) {

                        if (currentPage > 1) {
                            usersList.removeAt(usersList.size - 1)
                            adapter?.notifyItemRemoved(usersList.size)
                        } else {
                            usersList.clear()
                            adapter?.notifyDataSetChanged()
                        }

                    it.data?.let {
                        usersList.addAll(it)
                    }

                        totalPage = it.meta?.pagination?.total!!

                        adapter?.notifyDataSetChanged()
                        scrollListener?.setLoading()
                }
            })

            mSnackbarText.observe(this@UsersListFragment, Observer {
                Toast.makeText(context,it,Toast.LENGTH_SHORT).show()
            })
        }
    }


    private fun loadUsers() {
        viewModel.loadUsers(currentPage)
    }

    companion object {

        val FRAGMENT_NAME = UsersListFragment::class.java.name
    }
}