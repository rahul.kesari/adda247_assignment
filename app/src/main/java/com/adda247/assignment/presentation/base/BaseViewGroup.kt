package com.adda247.assignment.presentation.base

import androidx.databinding.ViewDataBinding

interface BaseViewGroup<V : BaseViewModel, B : ViewDataBinding> {

    val viewModel: V

    val layoutId: Int

    var binding: B

}
