package com.adda247.assignment.presentation.base

import androidx.fragment.app.Fragment
import com.adda247.assignment.util.extensions.TransitionAnimation

interface NavigationListener {

    fun navigateTo(
        fragment: Fragment,
        backStackTag: String,
        animationType: TransitionAnimation,
        isAdd: Boolean
    )

}