package com.adda247.assignment.presentation.base

import androidx.fragment.app.FragmentActivity

interface OnBackPressedListener {

    fun onBackPressed(activity: FragmentActivity) : Boolean

}
