package com.adda247.assignment

import android.app.Application
import android.content.Context
import androidx.multidex.MultiDex
import com.adda247.assignment.di.module.DatabaseModule
import com.adda247.assignment.di.module.HomeModule
import com.adda247.assignment.di.module.NetworkModule
import com.adda247.assignment.util.AppConstants
import com.adda247.assignment.util.helper.LocaleHelper
import com.facebook.stetho.Stetho
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class MainApplication : Application() {

    private val TAG = MainApplication::class.java.name
    companion object {
        lateinit var context: Context
    }

    override fun onCreate() {
        super.onCreate()
        context = applicationContext

        MultiDex.install(this)
        Stetho.initializeWithDefaults(this)

        startKoin {
            androidLogger()
            androidContext(this@MainApplication)
            modules(listOf(HomeModule, NetworkModule, DatabaseModule))
        }
    }

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(LocaleHelper.onAttach(base, AppConstants.DEFAULT_LANGUAGE))
        MultiDex.install(this)
    }

}